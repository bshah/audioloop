/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2020 Shah Bhushan <bshah@kde.org>
 */

#include <QCoreApplication>
#include <QDebug>
#include <QCommandLineParser>

#include "ofonowatcher.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    OfonoWatcher watcher;
    return a.exec();
}
