/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2020 Shah Bhushan <bshah@kde.org>
 */

#include "ofonowatcher.h"
#include "operation.h"

#include <pulse/pulseaudio.h>

static void pa_load_module_cb( pa_context *context, uint32_t index, void *userdata)
{
    if (index == PA_INVALID_INDEX) {
        qWarning() << "Failure to make loopback" << pa_strerror(pa_context_errno(context));
        return;
    } else {
        qDebug() << "Loopback tracked" << index;
        static_cast<OfonoWatcher *>(userdata)->trackLoopback(index);
    }
}

OfonoWatcher::OfonoWatcher(QObject *parent) : QObject(parent)
{
    m_voiceCallManager = new QOfonoVoiceCallManager(this);
    //TODO: hardcode, remove
    m_voiceCallManager->setModemPath("/quectelqmi_0");
    qDebug() << "voicecall manager is nice (assuming so)";
    QObject::connect(m_voiceCallManager, &QOfonoVoiceCallManager::callAdded,
                     this, &OfonoWatcher::callAdded);

    if (!QByteArray(QAbstractEventDispatcher::instance()->metaObject()->className()).contains("Glib")) {
        qWarning() << "Can't find glib loop";
        return;
    }
    qDebug() << "Going pulseaudio";
    m_mainloop = pa_glib_mainloop_new(nullptr);
    Q_ASSERT(m_mainloop);
    pa_mainloop_api *api = pa_glib_mainloop_get_api(m_mainloop);
    Q_ASSERT(api);
    m_context = pa_context_new(api, "audiolooper");
    Q_ASSERT(m_context);
    qDebug() << "pulse fine";

    if (pa_context_connect(m_context, NULL, PA_CONTEXT_NOFAIL, nullptr) < 0) {
        pa_context_unref(m_context);
        pa_glib_mainloop_free(m_mainloop);
        m_context = nullptr;
        m_mainloop = nullptr;
        return;
    }
}

void OfonoWatcher::callAdded(QString callId)
{
    m_voiceCall = new QOfonoVoiceCall(this);
    m_voiceCall->setVoiceCallPath(callId);

    qDebug() << "call fine (assuming)";
    connect(m_voiceCall, &QOfonoVoiceCall::stateChanged,
            this, &OfonoWatcher::callStateChanged);
}

void OfonoWatcher::callStateChanged(QString state)
{
    if (!m_context && !m_mainloop) {
        qWarning() << "Could not connect to pulse audio, returning";
        return;
    }
    if (state == QStringLiteral("active")) {
        qDebug() << "Foo";
        if(!PAOperation(pa_context_load_module(m_context, "module-loopback", "source=alsa_input.platform-sound.HiFi__hw_sun50ia64audio_0__source sink=alsa_output.platform-sound-modem.mono-fallback", pa_load_module_cb, this))) {
            qWarning() << "Failure to set the A64 -> modem loopback";
            return;
        }
        qDebug() << "A64->modem";
        if(!PAOperation(pa_context_load_module(m_context, "module-loopback", "source=alsa_input.platform-sound-modem.mono-fallback sink=alsa_output.platform-sound.HiFi__hw_sun50ia64audio_0__sink", pa_load_module_cb, this))) {
            qWarning() << "Failure to set the A64 <- modem loopback";
            return;
        }
        qDebug() << "A64<-modem";
    }
    if (state == QStringLiteral("disconnected")) {
        for(quint32 index : loop_index) {
            // we don't really need a callback here
            if(!PAOperation(pa_context_unload_module(m_context, index, nullptr, nullptr))) {
                return;
            }
            qDebug() << "Killing the " << index;
        }
        loop_index.clear();
    }
}

void OfonoWatcher::trackLoopback(quint32 index)
{
    loop_index << index;
}
