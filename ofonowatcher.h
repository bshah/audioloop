/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2020 Shah Bhushan <bshah@kde.org>
 */

#ifndef OFONOWATCHER_H
#define OFONOWATCHER_H

#include <QObject>

#include <pulse/glib-mainloop.h>
#include <pulse/context.h>
#include <pulse/ext-stream-restore.h>
#include <pulse/glib-mainloop.h>
#include <pulse/introspect.h>

#include "qofonovoicecallmanager.h"
#include "qofonovoicecall.h"

#include <QList>

// Main action of this class is to,
// 1) watch for new voice calls using libqofono
// 2) when we get a new call, wait for it to be accepted
// 3) once it is accepted, start pulseaudio loopbacks
// 4) if call is hung up, drop pulseaudio loopbacks

class OfonoWatcher : public QObject
{
    Q_OBJECT

public:
    explicit OfonoWatcher(QObject *parent = nullptr);
    void trackLoopback(quint32 index);

private:
    QOfonoVoiceCallManager *m_voiceCallManager;
    QOfonoVoiceCall *m_voiceCall;

    pa_context *m_context;
    pa_glib_mainloop *m_mainloop;

    QList<quint32> loop_index;

private slots:
    void callAdded(QString call);
    void callStateChanged(QString state);

};

#endif // OFONOWATCHER_H
